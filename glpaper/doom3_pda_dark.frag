#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 resolution;
uniform float time;

float grid(vec2 coord, float res){
    float c1 = step(0.9, -(1.-mod(coord.x, res)));
    float c2 = step(0.9, -(1.-mod(coord.y, res)));
    return 1. - c1 * c2;
}

vec3 blend(vec4 bg, vec4 fg){
    float r = fg[0] * fg[3] + bg[0] * (1.0 - fg[3]);
    float g = fg[1] * fg[3] + bg[1] * (1.0 - fg[3]);
    float b = fg[2] * fg[3] + bg[2] * (1.0 - fg[3]);
    return vec3(r,g,b);
}

void main(){
  // Gradient
  vec2 st = gl_FragCoord.xy/resolution.xy;
  float st2 = gl_FragCoord.y/resolution.y;

  vec3 c1 = vec3(0.171, 0.171, 0.171);
  vec3 c2 = vec3(0.0312, 0.0312, 0.0312);

  float mixValue = st.y;
  vec3 bg = mix(c1,c2,mixValue);

  // Grid
  float size = 30.;
  vec4 g = vec4(0.0,0.0,0.0,0.0);
  vec2 grid_t = gl_FragCoord.xy + vec2(15);
  g += vec4(0.615,1.,1., grid(grid_t,150.) * 0.02);
  g += vec4(0.615,1.,1., grid(grid_t,30.) * 0.01);

  gl_FragColor = vec4(blend(vec4(bg,1.0), g), 1.0);

}


#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

// A grid of semi-transparent lines
float grid(vec2 coord, float res){
  float c1 = step(0.9, -(1.-mod(coord.x, res)));
  float c2 = step(0.9, -(1.-mod(coord.y, res)));
  return 1. - c1 * c2;
}

// Blending function
vec3 blend(vec4 bg, vec4 fg){
  float r = fg[0] * fg[3] + bg[0] * (1.0 - fg[3]);
  float g = fg[1] * fg[3] + bg[1] * (1.0 - fg[3]);
  float b = fg[2] * fg[3] + bg[2] * (1.0 - fg[3]);
  return vec3(r,g,b);
}

// A half-circle function
float half_circle(float radius, float x){
  return sqrt(max(pow(radius, 2.0) - pow(x, 2.0), 0.0));
}

uniform float time;
uniform int pointerCount;
uniform vec3 pointers[10];
uniform vec2 resolution;

void main(void) {
  // Generate Gradient Background
  vec2 st = gl_FragCoord.xy/resolution.xy;

  vec3 c1 = vec3(0.30, 0.30, 0.30);
  vec3 c2 = vec3(0.10, 0.10, 0.10);

  float mixValue = st.y;
  vec3 bg = mix(c1,c2,mixValue);

  // Generate Grid
  float size = 30.;
  vec4 g = vec4(0.0,0.0,0.0,0.0);
  vec2 grid_t = gl_FragCoord.xy + vec2(15);
  g += vec4(0.615,1.,1., grid(grid_t,180.) * 0.03);
  g += vec4(0.615,1.,1., grid(grid_t,30.) * 0.015);

  vec4 wp = vec4(blend(vec4(bg,1.0), g), 1.0);

  // Generate light pulse
  float pulseDuration = 5.0;
  float pulse_y = resolution.y - mod(time, pulseDuration) / pulseDuration * resolution.y;

  float pulse_opacity = half_circle(0.02*resolution.y, gl_FragCoord.y - pulse_y)/resolution.y;
  vec4 pulse = vec4(1.0,1.0,1.0,pulse_opacity);

  gl_FragColor = vec4(blend(wp, pulse), 1.0);
}

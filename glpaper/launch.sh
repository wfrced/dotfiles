#!/bin/bash
killall glpaper
cd ~/.local/share/glpaper
glpaper -f 60 -F eDP-1 doom3_pda.frag
glpaper -f 60 -F DP-1 doom3_pda.frag
glpaper -f 60 -F DP-2 doom3_pda.frag
glpaper -f 60 -F DP-3 doom3_pda.frag
glpaper -f 60 -F HDMI-A-1 doom3_pda.frag

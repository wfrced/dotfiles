# Required fonts
Droid Sans, Font Awesome and Iosevka.  
# Required software
`qt5ct` and `export QT_QPA_PLATFORMTHEME=qt5ct` in bash profile for proper Qt theme.  
# Problems
Some applications require `GDK_BACKEND=x11`.
Remove Fontconfig configuration if you don't want Iosevka as your only monospace font.
# Setup
Simply run ./setup.sh, it will `stow` everything.

#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias vim='nvim'
alias sudo='sudo '
export VISUAL='nvim'

# QoL aliases
alias kcn='kubectl config use-context '
# HW-accelerated Qutebrowser
alias qba='qutebrowser --qt-flag ignore-gpu-blacklist --qt-flag enable-gpu-rasterization --qt-flag enable-native-gpu-memory-buffers --qt-flag num-raster-threads=4'

# Keyboard layouts
export XKB_DEFAULT_LAYOUT=us,ru
export XKB_DEFAULT_OPTIONS=grp:alt_shift_toggle

# Tell applications to use Wayland
export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export GDK_BACKEND=wayland
#export SDL_VIDEODRIVER=wayland

export QT_QPA_PLATFORMTHEME=qt5ct
export TERM='xterm-256color'
export TERMINAL=alacritty

PS1='\[\033[1;32m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;34m\]\w\[\033[1;33m\]\$\[\033[0m\] '
# Dotfiles clutter removal
export XDG_DATA_HOME="$HOME/.local/share"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export GEM_HOME="$XDG_DATA_HOME"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export HISTFILE="$XDG_CACHE_HOME"/bash/.bash_history

source auth
set header_cache = "~/.local/share/mutt/cache/headers"
set message_cachedir = "~/.local/share/mutt/cache/bodies"
set certificate_file = "~/.local/share/mutt/certificates"

set editor = "nvim"

### Keybindings ###
bind index g noop
macro index,pager S \
    "<save-message>=INBOX.Spam<enter>" "Spam"
macro index,pager I \
    "<save-message>=INBOX<enter>" "Inbox"
macro index gi \
    "<change-folder>=INBOX<enter>" "Go to inbox"
macro index gt \
    "<change-folder>=INBOX.Sent<enter>" "Go to sent Mail"
macro index gd \
    "<change-folder>=INBOX.Drafts<enter>" "Go to drafts"
macro index gs \
    "<change-folder>=INBOX.Spam<enter>" "Go to Spam"
# Vi-like keybindings
bind  generic        l select-entry
macro index          h '<change-folder>?'
bind  index,pager    H display-toggle-weed
bind  pager          h exit
bind  pager          j next-line
bind  pager          k previous-line
bind  pager          l view-attachments
bind  attach,compose l view-attach
bind  attach         h exit
bind  attach         H display-toggle-weed
bind  compose        l view-attach
# Sidebar
bind  index,pager    \CP sidebar-prev
bind  index,pager    \CN sidebar-next
bind  index,pager    \CO sidebar-open
macro index,pager    b   '<enter-command>toggle sidebar_visible<enter>'
bind  index          B   bounce-message
# Other
bind  index          G group-reply
bind  editor         <delete>  delete-char

### Sorting ###
set sort     = 'threads'
set sort_aux = 'reverse-last-date-received'

### Charset ###
#set charset      = "utf-8"
#set locale       = `echo "${LC_ALL:-${LC_TIME:-${LANG}}}"`
#set send_charset = "us-ascii:iso-8859-15:utf-8"

### Colors ###
# General colors
color hdrdefault color110 color234
color quoted     color244 color234
color signature  color150 color234
color attachment color110 color234
color message    color252 color234
color error      color15  color1
color indicator  color252 color237
color status     color15  color239
color tree       color174 color234
color normal     color252 color234
color search     color0   color149
color tilde      color247 color234
color index      color180 color234 "~F"    # marked
color index      color150 color234 "~N|~O" # unread
color index      color15  color1   "~D"    # deleted
color index      color255 color96  "~T"    # toggled
color sidebar_new color150 color234
# Special headers
color header     color225 color234 '^Subject:'
# Special chars
color body       color244 color234 '\\[0-9]{3}'
color body       color244 color234 '&[#a-z0-9]+;'
# URLs
color body       color174 color234 '(\w+://|www\.)[-.a-z0-9]+\.[a-z]{2,}[A-Za-z0-9;:.~_/%@#?+&=-]*'
color body       color229 color234 '[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,4}'
# Lines
color body       color150 color234 '[-=+#_*~]{3,}.*'
color body       color150 color234 '^[#~].*'
color body       color150 color234 '^[=-].*[=-]$'
# Link-Tags
color body       color181 color234 '\[([0-9]+|IMG)\]'
# Attribution lines
color body       color248 color234 '(^|[^[:alnum:]])([Oo]n)? .*wrote:$'
color body       color248 color234 '(^|[^[:alnum:]])([Oo]n)? .* was blubbering:$'
color body       color248 color234 '(^|[^[:alnum:]])([Oo]n)? .* blubbered:$'
color body       color248 color234 '(^|[^[:alnum:]])[0-9]{4}/[0-9]{1,2}/[0-9]{1,2} .*:$'
color body       color248 color234 '(^|[^[:alnum:]])Am.*\+[0-9]{4}$'
color body       color248 color234 '(^|[^[:alnum:]])schrieb .* <.*@.*>:$'
color body       color248 color234 '(^|[^[:alnum:]])\* .* \[[0-9 -:+]*\]:$'
# PGP
color body       color146 color234 '^gpg.*'
color body       color146 color234 '^Primary key fingerprint:.*'
color body       color146 color234 '^     Subkey fingerprint:.*'
color body       color150 color234 '^gpg: Good signature .*'
color body       brightwhite red   '^gpg: BAD signature from.*'
color body       color229 color234 '^gpg: WARNING.*'
color body       color229 color234 '^gpg:          There is no indication that the signature belongs to the owner.'
color body       color229 color234 '^gpg: no valid OpenPGP data found.*'
# OpenSSL
color body       green color234    '^Verification successful'
color body       brightwhite red   '^Verification failure'
color body       yellow color234   '^Error reading S/MIME message'
# *bold*
color body       color229 color234 '(^|\W)[\*]\w.*\w[\*]($|\W)'
# _underlined_
color body       color229 color234 '(^|\W)_\w[[:alpha:]]*\w_($|\W)'
# /italics/
color body       color229 color234 '(^|\W)/\w[^/]*\w/($|\W)'

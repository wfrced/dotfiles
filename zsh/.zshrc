source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/usr/share/oh-my-zsh"

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="powerlevel10k/powerlevel10k"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load?
plugins=(colorize docker kubectl terraform aws)

# asdf-vm
. /opt/asdf-vm/asdf.sh

source $ZSH/oh-my-zsh.sh

# SSH
export SSH_KEY_PATH="~/.ssh/id_rsa"

alias kc='kubie ctx'
alias kn='kubie ns'
alias kb='kubebuilder'

# Yandex Cloud
alias yct='export YC_TOKEN=$(yc iam create-token) && export YC_SERVICE_ACCOUNT_KEY_FILE='

# Android Emulator
alias emul='(sleep 1; GDK_BACKEND=x11 QT_QPA_PLATFORM=xcb ~/Android/Sdk/emulator/emulator -avd Pixel_3_API_30 &)'

# Add common paths
export PATH="$GEM_HOME/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$GOPATH/bin:$PATH"
export PATH="$HOME/_Work/_TOOLS/akka-cli/bin:$PATH"
export PATH="$HOME/_Work/_PRODOPS/scripts/kubernetes/smartdrain:$PATH"
export PATH="$HOME/_Work/_TOOLS/pan-globalprotect-okta:$PATH"

# Use qt5ct theme switcher
export QT_QPA_PLATFORMTHEME=qt5ct
export TERMINAL=alacritty
export TERM=xterm-256color
export VISUAL=lvim
export EDITOR=lvim

# fluxctl namespace
export FLUX_FORWARD_NAMESPACE=flux

# ansible-vault
ANSIBLE_VAULT_PASSWORD_FILE="$HOME/_Work/_TOOLS/ansible-vault/vault"

# Allow go to work without modules
export GO111MODULE=auto

# Load autocompletions
autoload -U +X compinit && compinit
autoload -U +X bashcompinit && bashcompinit

# man color settings
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# QoL aliases
alias yay=paru
alias ap=ansible-playbook
alias cilium='cilium-cli'
alias sudo='sudo '
alias vim=lvim
alias k=kubectl
alias f=fluxctl
alias a=argocd
alias tf=terraform
alias tg=terragrunt
alias tga='tg apply'
alias tgaa='tg apply -auto-approve'
alias tgv='tg validate'
alias tgf='tg format'
alias tghf='tg hclfmt'
alias tgd='tg destroy'
alias tgp='tg plan'
alias tfaa='terraform apply -auto-approve '
alias step=step-cli
alias lg=lazygit
alias hd='lvim -d <(helm show values $1) $2'
alias youtube-dl='yt-dlp'
alias okta='OPENSSL_CONF=$HOME/_Work/openssl.conf gp-okta.py $HOME/_Work/_TOOLS/pan-globalprotect-okta/config.conf'
alias glab_download='for repo in $(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$1?include_subgroups=true" | jq -r ".projects[].ssh_url_to_repo"); do git clone $repo; done;'
alias kcv='asdf global kubectl $1'
alias pip='noglob pip'

# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	exec sway
fi

#export PATH="$HOME/.pyenv/bin:$PATH"
#export PATH="$(pyenv root)/shims:$PATH"
#eval "$(pyenv init -)"
#eval "$(pyenv virtualenv-init -)"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

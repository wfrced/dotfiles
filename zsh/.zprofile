# Breaks oh-my-zsh if not set
export LC_ALL=en_US.UTF-8

# Dotfiles clutter removal
# https://wiki.archlinux.org/index.php/XDG_Base_Directory
#
# XDG
export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export XDG_DESKTOP_DIR="$HOME/Documents/Desktop"
export XDG_DOWNLOAD_DIR="$HOME/Media/Downloads"
export XDG_MUSIC_DIR="$HOME/Media/Music"
export XDG_PICTURES_DIR="$HOME/Media/Pictures"
export XDG_VIDEOS_DIR="$HOME/Media/Videos"
# Config
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PACKER_CONFIG_DIR="$XDG_CONFIG_HOME/packer"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"
# Data
export GOPATH="$XDG_DATA_HOME/go"
export GEM_HOME="$XDG_DATA_HOME/gem"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export MINIKUBE_HOME="$XDG_DATA_HOME/minikube"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export ELECTRUMDIR="$XDG_DATA_HOME/electrum"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME/vagrant/aliases"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export HISTFILE="$XDG_DATA_HOME/zsh/history"
# Cache
export GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
export PACKER_CACHE_DIR="$XDG_CACHE_HOME/packer"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export PSQL_HISTORY="$XDG_CACHE_HOME/pg/psql_history"

export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"

export GRIM_DEFAULT_DIR="$XDG_PICTURES_DIR/screenshots"

# Disable less history
export LESSHISTFILE=-

# Keyboard layouts (terminal)
export XKB_DEFAULT_LAYOUT=us,ru
export XKB_DEFAULT_OPTIONS=grp:alt_shift_toggle

# fucking emojis man
export MINIKUBE_IN_STYLE=false

# These can be set in a systemd unit for Sway
# but I also use Void Linux and Gentoo sometimes
# so I leave it cross-platform
export GDK_BACKEND=wayland
export MOZ_ENABLE_WAYLAND=1
export QT_QPA_PLATFORM=wayland
export CLUTTER_BACKEND=wayland
export ECORE_EVAS_ENGINE=wayland-egl
export ELM_ENGINE=wayland_egl
export SDL_VIDEODRIVER=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export NO_AT_BRIDGE=1

# LibreOffice QT backend
export SAL_USE_VCLPLUGIN=gtk3

# AWS default profile
export AWS_DEFAULT_PROFILE=015720975394_prodops-fulladmin

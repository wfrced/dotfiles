# Bindings for normal mode
config.bind('<Ctrl+m>', ' hint links spawn -d mpv {hint-url} --ao=jack')
config.bind('<Ctrl+А>', 'scroll-page 0 1')
config.bind('<Ctrl+В>', 'scroll-page 0 0.5')
config.bind('<Ctrl+Г>', 'scroll-page 0 -0.5')
config.bind('<Ctrl+И>', 'scroll-page 0 -1')
config.bind('<А>', 'hint')
config.bind('<Д>', 'scroll right')
config.bind('<Й>', 'tab-close')
config.bind('<Л>', 'scroll up')
config.bind('<О>', 'scroll down')
config.bind('<Р>', 'scroll left')
config.bind('<Ш>', 'enter-mode insert')
config.bind('M', 'hint links spawn -d mpv {hint-url}')
config.bind('m', 'spawn -d mpv {url}')
config.bind('xjf', 'set content.javascript.enabled false')
config.bind('xjn', 'set content.javascript.enabled true')

config.unbind('b', mode='normal')
config.unbind('v', mode='normal')

# Bindings for insert mode
config.bind('<Ctrl+Shift+u>', 'spawn --userscript qute-keepassxc --key 56428A0A0F088D26F3B346273809D5A6C484CBD0', mode='insert')

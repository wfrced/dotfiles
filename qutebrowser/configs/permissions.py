# Allow websites to share screen content.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.desktop_capture = 'ask'

# Allow websites to request geolocations.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.geolocation = 'ask'

# Allow websites to record audio.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.media.audio_capture = 'ask'

# Allow websites to record audio and video.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.media.audio_video_capture = 'ask'

# Allow websites to record video.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.media.video_capture = 'ask'

# Allow websites to show notifications.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask
c.content.notifications.enabled = 'ask'


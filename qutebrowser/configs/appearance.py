color_black = '#dde2e7'
color_bg0 = '#444444'
color_bg1 = '#d0d0d0'
color_bg2 = '#333333'
color_bg4 = '#505050'
color_bg_grey = '#d0d0d0'
color_bg_red = '#e17373'
color_fg = '#d0d0d0'
color_red = '#ff2828'
color_yellow = '#b58900'
color_green = '#abd400'
color_cyan = '#3a8b84'
color_blue = '#5079be'
color_purple = '#b05ccc'
color_grey = '#333333'
color_grey_dim = '#bac3cb'
color_transparent = '#00000000'
# When/how to show the scrollbar.
# Type: String
# Valid values:
#   - always: Always show the scrollbar.
#   - never: Never show the scrollbar.
#   - when-searching: Show the scrollbar when searching for text in the webpage. With the QtWebKit backend, this is equal to `never`.
#   - overlay: Show an overlay scrollbar. On macOS, this is unavailable and equal to `when-searching`; with the QtWebKit backend, this is equal to `never`. Enabling/disabling overlay scrollbars requires a restart.
c.scrolling.bar = 'overlay'

# When to show the tab bar.
# Type: String
# Valid values:
#   - always: Always show the tab bar.
#   - never: Always hide the tab bar.
#   - multiple: Hide the tab bar if only one tab is open.
#   - switching: Show the tab bar when switching tabs.
c.tabs.show = 'always'

# Format to use for the tab title. The following placeholders are
# defined:  * `{perc}`: Percentage as a string like `[10%]`. *
# `{perc_raw}`: Raw percentage, e.g. `10`. * `{current_title}`: Title of
# the current web page. * `{title_sep}`: The string `" - "` if a title
# is set, empty otherwise. * `{index}`: Index of this tab. *
# `{aligned_index}`: Index of this tab padded with spaces to have the
# same   width. * `{id}`: Internal tab ID of this tab. * `{scroll_pos}`:
# Page scroll position. * `{host}`: Host of the current web page. *
# `{backend}`: Either `webkit` or `webengine` * `{private}`: Indicates
# when private mode is enabled. * `{current_url}`: URL of the current
# web page. * `{protocol}`: Protocol (http/https/...) of the current web
# page. * `{audio}`: Indicator for audio/mute status.
# Type: FormatString
c.tabs.title.format = '{audio}{perc}{current_title}'

# Width (in pixels or as percentage of the window) of the tab bar if
# it's vertical.
# Type: PercOrInt
c.tabs.width = '8%'

# Maximum width (in pixels) of tabs (-1 for no maximum). This setting
# only applies when tabs are horizontal. This setting does not apply to
# pinned tabs, unless `tabs.pinned.shrink` is False. This setting may
# not apply properly if max_width is smaller than the minimum size of
# tab contents, or smaller than tabs.min_width.
# Type: Int
c.tabs.max_width = -1

# Width (in pixels) of the progress indicator (0 to disable).
# Type: Int
c.tabs.indicator.width = 0

# Wrap when changing tabs.
# Type: Bool
c.tabs.wrap = True
# Position of the tab bar.
# Type: Position
# Valid values:
#   - top
#   - bottom
#   - left
#   - right
c.tabs.position = 'left'

# Position of the status bar.
# Type: VerticalPosition
# Valid values:
#   - top
#   - bottom
c.statusbar.position = 'bottom'

# Height (in pixels or as percentage of the window) of the completion.
# Type: PercOrInt
c.completion.height = '30%'

# Shrink the completion to be smaller than the configured size if there
# are no scrollbars.
# Type: Bool
c.completion.shrink = True

# Width (in pixels) of the scrollbar in the completion window.
# Type: Int
c.completion.scrollbar.width = 24

# CSS border value for hints.
# Type: String
c.hints.border = '1px solid '+ color_fg

# Text color of the completion widget. May be a single color to use for
# all columns or a list of three colors, one for each column.
# Type: List of QtColor, or QtColor
c.colors.completion.fg = color_fg

# Background color of the completion widget for odd rows.
# Type: QssColor
c.colors.completion.odd.bg = color_bg0

# Background color of the completion widget for even rows.
# Type: QssColor
c.colors.completion.even.bg = color_bg2

# Foreground color of completion widget category headers.
# Type: QtColor
c.colors.completion.category.fg = color_bg1

# Background color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.bg = color_grey

# Top border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.top = color_fg

# Bottom border color of the completion widget category headers.
# Type: QssColor
c.colors.completion.category.border.bottom = color_fg

# Foreground color of the selected completion item.
# Type: QtColor
c.colors.completion.item.selected.fg = color_bg2

# Background color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.bg = color_bg_grey

# Top border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.top = color_grey

# Bottom border color of the selected completion item.
# Type: QssColor
c.colors.completion.item.selected.border.bottom = color_grey

# Foreground color of the matched text in the completion.
# Type: QtColor
c.colors.completion.match.fg = color_red

# Color of the scrollbar handle in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.fg = color_fg

# Color of the scrollbar in the completion view.
# Type: QssColor
c.colors.completion.scrollbar.bg = color_bg2

# Background color for the download bar.
# Type: QssColor
c.colors.downloads.bar.bg = color_bg2

# Color gradient start for download text.
# Type: QtColor
c.colors.downloads.start.fg = color_bg1

# Color gradient start for download backgrounds.
# Type: QtColor
c.colors.downloads.start.bg = color_cyan

# Color gradient end for download text.
# Type: QtColor
c.colors.downloads.stop.fg = color_bg0

# Color gradient stop for download backgrounds.
# Type: QtColor
c.colors.downloads.stop.bg = color_green

# Foreground color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.fg = color_bg0

# Background color for downloads with errors.
# Type: QtColor
c.colors.downloads.error.bg = color_red

# Font color for hints.
# Type: QssColor
c.colors.hints.fg = color_fg

# Background color for hints. Note that you can use a `rgba(...)` value
# for transparency.
# Type: QssColor
c.colors.hints.bg = color_bg0

# Font color for the matched part of hints.
# Type: QtColor
c.colors.hints.match.fg = color_red

# Text color for the keyhint widget.
# Type: QssColor
c.colors.keyhint.fg = color_fg

# Highlight color for keys to complete the current keychain.
# Type: QssColor
c.colors.keyhint.suffix.fg = color_yellow

# Background color of the keyhint widget.
# Type: QssColor
c.colors.keyhint.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 '+color_bg4+', stop:1 '+color_bg1+')'

# Foreground color of an error message.
# Type: QssColor
c.colors.messages.error.fg = color_red

# Background color of an error message.
# Type: QssColor
c.colors.messages.error.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 '+color_bg4+', stop:1 '+color_bg1+')'

# Border color of an error message.
# Type: QssColor
c.colors.messages.error.border = color_red

# Foreground color of a warning message.
# Type: QssColor
c.colors.messages.warning.fg = color_fg

# Foreground color of an info message.
# Type: QssColor
c.colors.messages.info.fg = color_bg2

# Background color of an info message.
# Type: QssColor
c.colors.messages.info.bg = color_bg1

# Border color of an info message.
# Type: QssColor
c.colors.messages.info.border = color_fg

# Foreground color for prompts.
# Type: QssColor
c.colors.prompts.fg = color_fg

# Border used around UI elements in prompts.
# Type: String
c.colors.prompts.border = color_fg

# Background color for prompts.
# Type: QssColor
c.colors.prompts.bg = color_bg4

# Background color for the selected item in filename prompts.
# Type: QssColor
c.colors.prompts.selected.bg = color_grey

# Foreground color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.fg = color_fg

# Background color of the statusbar.
# Type: QssColor
c.colors.statusbar.normal.bg = color_bg0

# Foreground color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.fg = color_yellow

# Background color of the statusbar in insert mode.
# Type: QssColor
c.colors.statusbar.insert.bg = color_bg2

# Foreground color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.fg = color_cyan

# Background color of the statusbar in passthrough mode.
# Type: QssColor
c.colors.statusbar.passthrough.bg = color_bg2

# Foreground color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.fg = color_fg

# Background color of the statusbar in command mode.
# Type: QssColor
c.colors.statusbar.command.bg = color_bg2

# Foreground color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.fg = color_purple

# Background color of the statusbar in caret mode.
# Type: QssColor
c.colors.statusbar.caret.bg = color_bg4

# Background color of the progress bar.
# Type: QssColor
c.colors.statusbar.progress.bg = color_purple

# Default foreground color of the URL in the statusbar.
# Type: QssColor
c.colors.statusbar.url.fg = color_fg

# Foreground color of the URL in the statusbar on error.
# Type: QssColor
c.colors.statusbar.url.error.fg = color_red

# Foreground color of the URL in the statusbar for hovered links.
# Type: QssColor
c.colors.statusbar.url.hover.fg = color_purple

# Foreground color of the URL in the statusbar on successful load
# (http).
# Type: QssColor
c.colors.statusbar.url.success.http.fg = color_fg

# Foreground color of the URL in the statusbar on successful load
# (https).
# Type: QssColor
c.colors.statusbar.url.success.https.fg = color_green

# Background color of the tab bar.
# Type: QssColor
c.colors.tabs.bar.bg = color_transparent

# Color gradient start for the tab indicator.
# Type: QtColor
c.colors.tabs.indicator.start = color_purple

# Color gradient end for the tab indicator.
# Type: QtColor
c.colors.tabs.indicator.stop = color_green

# Color for the tab indicator on errors.
# Type: QtColor
c.colors.tabs.indicator.error = color_red

# Foreground color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.fg = color_fg

# Background color of unselected odd tabs.
# Type: QtColor
c.colors.tabs.odd.bg = color_bg0

# Foreground color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.fg = color_fg

# Background color of unselected even tabs.
# Type: QtColor
c.colors.tabs.even.bg = color_bg2

# Foreground color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.fg = color_black

# Background color of selected odd tabs.
# Type: QtColor
c.colors.tabs.selected.odd.bg = color_grey

# Foreground color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.fg = color_black

# Background color of selected even tabs.
# Type: QtColor
c.colors.tabs.selected.even.bg = color_grey

# Font used in the completion widget.
# Type: Font
c.fonts.completion.entry = '12pt Droid Sans'

# Font used in the completion categories.
# Type: Font
c.fonts.completion.category = 'bold 12pt Droid Sans'

# Font used for the downloadbar.
# Type: Font
c.fonts.downloads = '12pt Droid Sans'

# Font used for the hints.
# Type: Font
c.fonts.hints = 'bold 12pt Droid Sans'

# Font used in the statusbar.
# Type: Font
c.fonts.statusbar = 'bold 12pt Droid Sans'

# Font used for selected tabs.
# Type: Font
c.fonts.tabs.selected = '14pt Droid Sans Bold'

# Font used for unselected tabs.
# Type: Font
c.fonts.tabs.unselected = '14pt Droid Sans'

# Default font size (in pixels) for regular text.
# Type: Int
c.fonts.web.size.default = 18

# Default font size (in pixels) for fixed-pitch text.
# Type: Int
c.fonts.web.size.default_fixed = 15


#!/bin/bash

# Generates a wallpaper with
# Windows Mobile 6-like gradient bar

# Defaults
WIDTH=1920
HEIGHT=1080
THICKNESS=34
COLOR=4c566a
FILL=2e3440
HIGHDPI=false
EXPORT="$HOME/.config/sway/"

while getopts ":whtfcde" option
do
  case "${option}" in
    w ) WIDTH=${OPTARG};;
    h ) HEIGHT=${OPTARG};;
    t ) THICKNESS=${OPTARG};;
    f ) FILL=${OPTARG};;
    c ) COLOR=${OPTARG};;
    d ) HIGHDPI=${OPTARG};;
    e ) EXPORT=${OPTARG};;
    \? ) echo "Usage: gradient.sh [-w]idth [-h]eight [-t]hickness [-f]ill [-c]olor [-d] [-e]"
  esac
done

let "THICKNESS_H=$THICKNESS / 2"
let "WIDTH_H=$WIDTH / 2"
let "HEIGHT_H=$HEIGHT / 2"
let "WP_HEIGHT=$HEIGHT - $THICKNESS"

magick -size "${WIDTH_H}x${THICKNESS_H}" -define gradient:direction=east gradient:"rgba(255, 255, 255, 0)"-"rgba(255, 255, 255, 0.24)" east.png
magick -size "${WIDTH_H}x${THICKNESS_H}" -define gradient:direction=North gradient:"rgba(255, 255, 255, 0.2)"-"rgba(255, 255, 255, 0.4)" south.png
magick -size "${WIDTH_H}x${THICKNESS_H}" -define gradient:direction=North gradient:"rgba(255, 255, 255, 0.0)"-"rgba(255, 255, 255, 0.1)" north.png
magick -size "${WIDTH_H}x${THICKNESS_H}" xc:"#$COLOR" fill.png
magick composite -compose Plus east.png south.png gradient_south.png
magick composite -compose Plus east.png north.png gradient_north.png
magick composite -compose LinearDodge gradient_south.png fill.png quarter1.png
magick composite -compose LinearDodge gradient_north.png fill.png quarter3.png
magick convert quarter1.png -flop quarter2.png
magick convert quarter3.png -flop quarter4.png
magick convert -append quarter1.png quarter3.png half_l.png
magick convert -append quarter2.png quarter4.png half_r.png
magick convert +append half_l.png half_r.png result.png
# Blur and noise are to remove banding
# I don't remove it from bar because I want it here
# for nostalgic reasons
magick -size "${WIDTH}x${WP_HEIGHT}" -define gradient:direction=North gradient:"#$COLOR"-"#$FILL" -quality 100 wp.png
magick convert -append result.png wp.png "$EXPORT/wallpaper.png"

mkdir -p highdpi
cd highdpi
let "D_WIDTH=$WIDTH * 2"
let "D_WP_HEIGHT=$WP_HEIGHT * 2"

magick -size "${WIDTH}x${THICKNESS}" -define gradient:direction=east gradient:"rgba(255, 255, 255, 0)"-"rgba(255, 255, 255, 0.3)" east.png
magick -size "${WIDTH}x${THICKNESS}" -define gradient:direction=North gradient:"rgba(255, 255, 255, 0.16)"-"rgba(255, 255, 255, 0.35)" south.png
magick -size "${WIDTH}x${THICKNESS}" -define gradient:direction=North gradient:"rgba(255, 255, 255, 0.0)"-"rgba(255, 255, 255, 0.1)" north.png
magick -size "${WIDTH}x${THICKNESS}" xc:"#$FILL" fill.png
magick composite -compose Plus east.png south.png gradient_south.png
magick composite -compose Plus east.png north.png gradient_north.png
magick composite -compose LinearDodge gradient_south.png fill.png quarter1.png
magick composite -compose LinearDodge gradient_north.png fill.png quarter3.png
magick convert quarter1.png -flop quarter2.png
magick convert quarter3.png -flop quarter4.png
magick convert -append quarter1.png quarter3.png half_l.png
magick convert -append quarter2.png quarter4.png half_r.png
magick convert +append half_l.png half_r.png result.png
magick -size "${D_WIDTH}x${D_WP_HEIGHT}" -define gradient:direction=North gradient:"#$FILL"-"#$COLOR" -quality 100 +noise Uniform -blur 0x1 -fx intensity wp.png
magick convert -append result.png wp.png "$EXPORT/wallpaper_highdpi.png"

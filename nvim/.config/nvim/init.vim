set number	
set showmatch	
set linebreak
set visualbell

set hlsearch	
set smartcase
set ignorecase	
set incsearch
 
set autoindent
set expandtab
set shiftwidth=2	
set smartindent	
set smarttab
set softtabstop=2	

set ruler	

set undolevels=1000	
set backspace=indent,eol,start	
colorscheme elflord

set cursorline
hi CursorLine term=none cterm=none ctermbg=3

call plug#begin('~/.local/share/nvim/plugged')

Plug 'itchyny/lightline.vim'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'w0rp/ale', { 'on': 'ALEToggle' }

" Initialize plugin system
call plug#end()

map <C-o> :NERDTreeToggle<CR>
map <C-p> :ALEToggle<CR>
map ; :Files<CR>

" Vim UI {

  highlight Comment gui=italic
  highlight String gui=italic

  set noshowmode " Don't display the current mode
  set showcmd
  set cmdheight=1

  set cursorline " Highlight current line
  hi CursorLine term=none cterm=none ctermbg=3
  hi LineNr ctermbg=darkgrey

  set linespace=0 " No extra spaces between rows
  set number " Line numbers on
  set showmatch " Show matching brackets/parenthesis
  set incsearch " Find as you type search
  set hlsearch " Highlight search terms
  set winminheight=0 " Windows can be 0 line high
  set ignorecase " Case insensitive search
  set smartcase " Case sensitive when uc present
  set wildmenu " Show list instead of just completing
  "set wildmode=list:longest,full " Command <Tab> completion, list matches, then longest common part, then all.
  set whichwrap=b,s,h,l,<,>,[,] " Backspace and cursor keys wrap too
  set scrolloff=5 " Minimum lines to keep above and below cursor
  set sidescroll=1 " Sane horizontal scrolling if nowrap is enabled
  set foldenable " Auto fold code
  set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

  set conceallevel=0 " Disable folding

  " always show signcolumns
  set signcolumn=yes

" }

" Formatting {

  set wrap " Wrap long lines
  set linebreak " Sane wrapping
  set breakindent " Indent wrapped lines
  set autoindent " Indent at the same level of the previous line
  set smartindent	" Additional checks for autoindent
  set shiftwidth=2 " Use indents of 4 spaces
  "set expandtab " Tabs are spaces, not tabs
  set smarttab
  "set softtabstop=2 " Amount of tabs inserted for autoformatting
  set splitbelow splitright " Puts new splits to the bottom/right of the current

  set formatoptions-=cro

" }

" Key (re)Mappings {

  " Wrapped lines goes down/up to next row, rather than next line in file.
  noremap j gj
  noremap k gk

  " Quick tab switching
  nnoremap H gT
  nnoremap L gt

  " Remap leader key to ,
  let g:mapleader=","

  " make <a-j>, <a-k>, <a-l>, and <a-h> move to window.
  nnoremap <a-j> <C-W>j
  nnoremap <a-k> <C-W>k
  nnoremap <a-h> <C-W>h
  nnoremap <a-l> <C-W>l
  " New split
  nnoremap <a-cr> <C-W>v

  map <C-n> :cnext<CR>
  map <C-p> :cprevious<CR>
  nnoremap <leader>a :cclose<CR>

  " Replace all is aliased to S.
  nnoremap S :%s//g<Left><Left>
  " Remap VIM 0 to first non-blank character
  map 0 ^

  " Visual shifting (does not exit Visual mode)
  vnoremap < <gv
  vnoremap > >gv

" }

if has('clipboard')
  if has('unnamedplus')  " When possible use + register for copy-paste
    set clipboard=unnamed,unnamedplus
  else         " On mac and Windows, use * register for copy-paste
    set clipboard=unnamed
  endif
endif

colorscheme wfrced-bright
